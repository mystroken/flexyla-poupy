<?php

/**
 * Poupy — A front-end starter kit for web development.
 * 
 * @package Poupy
 * @author Mystro Ken <mystroken@gmail.com>
 */ 

declare(strict_types=1);

/*
|---------------------------------------------------------
| Register the auto loader
|---------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

require __DIR__ . '/../vendor/autoload.php';

/*
|---------------------------------------------------------
| Run the application
|---------------------------------------------------------
|
|
*/

$request = $_SERVER['REQUEST_URI'];

switch ($request) {
    case '/' :
        require __DIR__ . '/../resources/views/home.php';
        break;
    case '' :
        require __DIR__ . '/../resources/views/home.php';
        break;
    case '/about' :
        require __DIR__ . '/../resources/views/about.php';
        break;
    default:
        require __DIR__ . '/../resources/views/404.php';
        break;
}